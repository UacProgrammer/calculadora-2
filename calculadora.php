<?php

    class Calculadora
    {
        //atributos
        public $nro1;
        public $nro2;

        //metodo
        public function Sumar()
        {  
            return $this->nro1 + $this->nro2;
        }
        public function Restar()
        {
            return $this->nro1 - $this->nro2;
        }
        public function Multiplicar()
        {
            return $this->nro1 * $this->nro2;
        }
        public function Dividir()
         {
            if($this->nro2 == 0)
            {
                echo "No se puede dividir entre 0";
            }
            else
            {
                return $this->nro1 / $this->nro2;
            }
                 
         }
         public function Factorial($nro)
         {
             if($nro == 0)
                 return 1;
             else
                 return $nro * $this->Factorial($nro - 1);
         }
         //potencia, seno y tangente
         public function Potencia($nro1,$nro2)
         {
             if($nro2 == 0)
                 return 1;
             else
                 return $nro1 * $this->Potencia($nro1,$nro2-1);
         }
         public function Seno($nro1)
         {
             return(sin(deg2rad($nro1)));
         }
         public function Coseno($nro1)
         {
           return (cos(($nro1 * pi()) / 180));
         }
         public function Tan($nro1)
         {
             return(tan(($nro1*pi()/180)));
         }
         public function Porcentaje()
         {
             return ($this->nro1*$this->nro2/100);
         }
         public function RaizCuadrada()
         {
            return (sqrt($this->nro1));
         }
         public function RaizNesima()
         {
            //return (rootrem($this->nro1,$this->nro2));
            return pow($this->nro1,1/$this->nro2);
         }
         public function Inverso()
         {
            return (1/$this->nro1);
         }
         /*public function RaizNesima()
         {
            if ($nro1 >= 2 and $nro2>0)
            {
                $nro1 * (1 / $nro2);
            }
                
         }*/

        }

?>