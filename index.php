<!DOCTYPE html>
<html lang="en">
<head style="font-size: 13px;">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Material Design for Bootstrap</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Google Fonts Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="css/mdb.min.css">
    <!-- Your custom styles (optional) -->
    <link rel="stylesheet" href="css/style.css">
    </head>
    <?php
    //Llamar a la clase calculadora
    if($_POST)
    {
    include("calculadora.php");
    
    $calculo = new Calculadora;
    $nro1 = $_POST['txtNro1'];
    $nro2 = $_POST['txtNro2'];
   
    $calculo->nro1 = $nro1;
    $calculo->nro2 = $nro2;
    
    echo "<input type='text' style='width:100%; background-color: #252525; text-align: center; color: #fff;' value='";
    if(isset($_POST['btnSumar']))
    {
        //Instanciar un objeto a traves de la clase
        echo "La suma de los números es: ",$calculo->Sumar();
    }
    if(isset($_POST['btnRestar']))
    {   
        //Instanciar un objeto a traves de la clase
        echo "La resta de los numeros es: ",$calculo->Restar();
    }
    if(isset($_POST['btnMultiplicar']))
    {
        //Instanciar un objeto a traves de la clase
        echo "La multiplicación de los numeros es: ",$calculo->Multiplicar();
    }
    if(isset($_POST['btnDividir']))
    {
        //Instanciar un objeto a traves de la clase
        echo "La division de los numeros es: ",$calculo->Dividir();
    }
    if(isset($_POST['btnPotencia']))
    {
        //Instanciar un objeto a traves de la clase
        echo "La potencia de los numeros es: ",$calculo->Potencia($nro1, $nro2);
    }
    if(isset($_POST['btnFactorial']))
    {
        //Instanciar un objeto a traves de la clase
        echo "El facorial del numero es: ",$calculo->Factorial($nro1);
    }
    if(isset($_POST['btnSeno']))
    {
        //Instanciar un objeto a traves de la clase
        echo "El seno del numero es: ",$calculo->Seno($nro1);
    }
    if(isset($_POST['btnCoseno']))
    {
        //Instanciar un objeto a traves de la clase
        echo "El coseno del numero es: ",$calculo->Coseno($nro1);
    }
    if(isset($_POST['btnTangente']))
    {
        //Instanciar un objeto a traves de la clase
        echo "La tangente del numero es: ",$calculo->Tan($nro1);
    }
    if(isset($_POST['btnPorcentaje']))//
    {
        //Instanciar un objeto a traves de la clase
        echo "El porcentaje del numero es: ",$calculo->Porcentaje($nro1);
    }
    if(isset($_POST['btnRaiz']))//
    {
        //Instanciar un objeto a traves de la clase
        echo "La raiz cuadrada del numero es: ",$calculo->RaizCuadrada($nro1);
    }
    if(isset($_POST['btnRaizNesima']))
    {
        //Instanciar un objeto a traves de la clase
        echo "La raiz n-esima es: ",$calculo->RaizNesima($nro1);
    }
    if(isset($_POST['btnInversa']))//
    {
        //Instanciar un objeto a traves de la clase
        echo "La inversa del numero es: ",$calculo->Inverso($nro1);
    }
    echo "'/>";
    
}
?>
<body>
    <div class="container my-4">  
<div class="calculator card">
  <form action="#" method="POST">
    <p>nro1: <input type="text" class="calculator-screen z-depth-1" name="txtNro1"/></p>
    <p>nro2: <input type="text" class="calculator-screen z-depth-1" name="txtNro2"/></p>

    <div class="calculator-keys">

      <input type="submit" class="operator btn btn-info" value="+" name="btnSumar"/>
      <input type="submit" class="operator btn btn-info" value="-" name="btnRestar">
      <input type="submit" class="operator btn btn-info" value="*" name="btnMultiplicar"/>
      <input type="submit" class="operator btn btn-info" value="/" name="btnDividir"/>
      <input type="submit" class="operator btn btn-info" value="^" name="btnPotencia"/>
      <input type="submit" class="operator btn btn-info" value="Fac" name="btnFactorial"/>
      <input type="submit" class="operator btn btn-info" value="Sen" name="btnSeno"/>
      <input type="submit" class="operator btn btn-info" value="Cos" name="btnCoseno"/>
      <input type="submit" class="operator btn btn-info" value="Tan" name="btnTangente"/>
      <input type="submit" class="operator btn btn-info" value="%" name="btnPorcentaje"/>
      <input type="submit" class="operator btn btn-info" value="Raiz" name="btnRaiz"/>
      <input type="submit" class="operator btn btn-info" value="R-ene" name="btnRaizNesima"/>
      <input type="submit" class="operator btn btn-info" value="Inver" name="btnInversa"/>
      <button type="button" class="all-clear function btn btn-danger btn-sm" value="all-clear">AC</button>
  </form>
      <button type="button" value="7" class="btn btn-light waves-effect" style="visibility: hidden;">7</button>
      <button type="button" value="7" class="btn btn-light waves-effect" style="visibility: hidden;">7</button>

      <button type="button" value="7" class="btn btn-light waves-effect">7</button>
      <button type="button" value="8" class="btn btn-light waves-effect">8</button>
      <button type="button" value="9" class="btn btn-light waves-effect">9</button>

      <br>
      <button type="button" value="4" class="btn btn-light waves-effect">4</button>
      <button type="button" value="5" class="btn btn-light waves-effect">5</button>
      <button type="button" value="6" class="btn btn-light waves-effect">6</button>
      <br>

      <button type="button" value="1" class="btn btn-light waves-effect">1</button>
      <button type="button" value="2" class="btn btn-light waves-effect">2</button>
      <button type="button" value="3" class="btn btn-light waves-effect">3</button>


      <button type="button" value="0" class="btn btn-light waves-effect">0</button>


    </div>
  </div>
</div>
    <!-- jQuery -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>
    <!-- Your custom scripts -->
    <script type="text/javascript" src="js/script.js"></script>
</body>
</html>