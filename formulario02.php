<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Procesar PHP dentro del mismo Formulario</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Google Fonts Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="css/mdb.min.css">
    <!-- Your custom styles (optional) -->
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div class="calculator-keys">
    <form action="#" method="POST" >
    <legend>Calculadora</legend>
    <p>Nro1: <input type="text" name = "txtNro1" /></p>
    <p>Nro2: <input type="text" name = "txtNro2"/></p>
    <p>
    <input type="submit" name = "btnSumar" value = "Sumar" class="operator btn btn-info"/>
    <input type="submit" name = "btnRestar" value = "Restar" class="operator btn btn-info"/>
    <input type="submit" name = "btnMultiplicar" value = "Multiplicar"/>
    <input type="submit" name = "btnDividir" value = "Dividir"/>
    <input type="submit" name = "btnPotencia" value = "Potencia"/>
    <input type="submit" name = "btnFactorial" value = "Factorial"/>
    <input type="submit" name = "btnSeno" value = "Seno"/>
    <input type="submit" name = "btnCoseno" value = "Tangente"/>
    <input type="submit" name = "btnRaizNesima" value = "RaizNesima"/>
    </p>
    </form>
    </div>
<?php 
    //Llamar a la clase calculadora
    if($_POST)
    {
    include("calculadora.php");
    
    $calculo = new Calculadora;
    $nro1 = $_POST['txtNro1'];
    $nro2 = $_POST['txtNro2'];
   
    $calculo->nro1 = $nro1;
    $calculo->nro2 = $nro2;
    echo "<p style='text-align: center;'>";
    if(isset($_POST['btnSumar']))
    {
        //Instanciar un objeto a traves de la clase
        echo "La suma de los números es: ",$calculo->Sumar();
    }
    if(isset($_POST['btnRestar']))
    {   
        //Instanciar un objeto a traves de la clase
        echo "La resta de los numeros es: ",$calculo->Restar();
    }
    if(isset($_POST['btnMultiplicar']))
    {
        //Instanciar un objeto a traves de la clase
        echo "La multiplicación de los numeros es: ",$calculo->Multiplicar();
    }
    if(isset($_POST['btnDividir']))
    {
        //Instanciar un objeto a traves de la clase
        echo "La division de los numeros es: ",$calculo->Dividir();
    }
    if(isset($_POST['btnPotencia']))
    {
        //Instanciar un objeto a traves de la clase
        echo "La potencia de los numeros es: ",$calculo->Potencia($nro1, $nro2);
    }
    if(isset($_POST['btnFactorial']))
    {
        //Instanciar un objeto a traves de la clase
        echo "El facorial del numero es: ",$calculo->Factorial($nro1);
    }
    if(isset($_POST['btnSeno']))
    {
        //Instanciar un objeto a traves de la clase
        echo "El seno del numero es: ",$calculo->Seno($nro1);
    }
    if(isset($_POST['btnCoseno']))
    {
        //Instanciar un objeto a traves de la clase
        echo "El coseno de numero es: ",$calculo->Tan($nro1);
    }
    if(isset($_POST['btnRaizNesima']))
    {
        //Instanciar un objeto a traves de la clase
        echo "La raiz n-esima es: ",$calculo->RaizNesima($nro1);
    }
    echo "<p/>";
    
}
?>
</body>
</html>